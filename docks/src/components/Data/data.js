import React, { Component } from "react";

export default class data extends Component {
  async componentDidMount() {
    const token = this.props.token;
    const res = await fetch("http://35.237.44.53:8069/api/docks", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    const data = await res.json();
    console.log(data);
  }
  render() {
    return <div />;
  }
}
