import React, { Component } from "react";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
export default class login extends Component {
  state = {
    dbname: "",
    username: "",
    password: ""
  };
  handleChange = ({ target: { value, name } }) => {
    this.setState({ [name]: value });
  };
  getData = async () => {
    const res = await fetch("http://35.237.44.53:8069/api/login", {
      method: "GET",
      headers: {
        dbname: this.state.dbname,
        username: this.state.username,
        password: this.state.password
      }
    });
    const data = await res.json();
    console.log(data);
  };
  render() {
    return (
      <div style={{ margin: "0 auto", marginTop: "10%" }}>
        <FormControl>
          <InputLabel htmlFor="dbname">DBname</InputLabel>
          <Input
            fullWidth
            type="text"
            name="dbname"
            value={this.state.name}
            onChange={this.handleChange}
          />
        </FormControl>
        <br />
        <FormControl>
          <InputLabel htmlFor="username">username</InputLabel>
          <Input
            fullWidth
            type="text"
            name="username"
            value={this.state.name}
            onChange={this.handleChange}
          />
        </FormControl>
        <br />
        <FormControl>
          <InputLabel htmlFor="password">password</InputLabel>
          <Input
            fullWidth
            type="password"
            name="password"
            value={this.state.name}
            onChange={this.handleChange}
          />
        </FormControl>
        <br />
        <br />

        <Button variant="contained" color="primary" onClick={this.getData}>
          Login
        </Button>
      </div>
    );
  }
}
