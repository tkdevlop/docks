import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

const newLocal = document.createElement("div");
it("renders without crashing", () => {
  const div = newLocal;
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
