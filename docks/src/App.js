import React, { Component } from "react";
import "./App.css";
import Login from "./components/Login";
import Data from "./components/Data";
class App extends Component {
  state = {
    isAuth: false,
    token: ""
  };
  setAuth = token => {
    this.setState({ isAuth: true, token });
  };
  render() {
    return (
      <div className="App">
        <Login toggleAuth={this.setAuth} />
        {this.state.isAuth ? <Data token={this.state.token} /> : null}
      </div>
    );
  }
}

export default App;
